var formSelect = Vue.component('form-select', {
    props: ['params'],
    template: `
        <select :id="params.id">
            <option
                v-for="option in params.options"
                :value="option.value"
                v-text="option.text">
            </option>
        </select>
    `,
});
