var formInputText = Vue.component('form-inputText', {
    props: ['params'],
    template: `
        <input type="text" :id="params.id" :placeholder="params.placeholder">
    `,
});
