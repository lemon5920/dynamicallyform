var formLabel = Vue.component('form-label', {
    props: ['params'],
    template: `
        <label :for="params.for" v-text="params.text"></label>
    `,
});
