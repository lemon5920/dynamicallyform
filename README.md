# DynamicallyForm

試做以 JSON 渲染表單，單純以 CDN 引入 Vue 實作，未使用 Vue CLI。
- Fetch 表單元素 JSON Data。
- 將每個表單元素組件化。
- JSON Data 中，每個物件都需要有 Key `type`（表單元素類型）、`params`（傳入組件參數）。
- 按照 JSON Data 內容，依序渲染各個表單元素。

```
.
+-- components
|   +-- formInputText.js
|   +-- formLabel.js
|   +-- formSelect.js
+-- index.html
```

## JSON Data
https://next.json-generator.com/api/json/get/4yhP19ctL

- 兩個 Text fields，參數：`placeholder`, `id`
- 一個 Select field，參數：`options`（選項顯示文字 `text` 與值 `value`）, `id`
- 三個 Label，參數：`text`, `for`

```javascript
[
  {
    type:'formLabel',
    params: {
      text: 'Your name',
      for: 'input--name',
    },
  },
  {
    type: 'input-text',
    params: {
      placeholder: 'input name',
      id: 'input--name',
    },
  },
  {
    type:'formLabel',
    params: {
      text: 'Your age',
      for: 'input--age',
    },
  },
  {
    type: 'input-text',
    params: {
      placeholder: 'input age',
      id: 'input--age',
    },
  },
  {
    type:'formLabel',
    params: {
      text: 'Your home',
      for: 'select-address',
    },
  },
  {
    type: 'selectbar',
    params: {
      options: [
        {text: '台北', value: 'taipei'},
        {text: '台中', value: 'taichung'},
        {text: '台南', value: 'tainan'},
      ],
      id: 'select-address',
    },
  },
]
```

## Result
https://lemon5920.gitlab.io/dynamicallyform/

![](https://i.imgur.com/fkGGOqC.png)